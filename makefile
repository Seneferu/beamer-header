# Precompiles the static header for a LaTeX Beamer presentation.

all:
	pdflatex -ini "&pdflatex beamer.tex\dump"
