# Beamer Header

This repository contains a static header for presentations using the LaTeX beamer class.

## Making a new Presentation

First, create a new repository for your presentation.
Clone this repository as submodule into your repository.

`git submodule add git@bitbucket.org:Seneferu/beamer-header.git header`

This will create a folder *header* insider you repository.
You can use any other name, but you need to update the code later.
Then go into the folder and run make.

`cd header && make`

Go back to the parent directory and create a file *main.tex* containing

    %&header/beamer
    
    \begin{document}
    \section{Hello World}
    \end{document}

The line `%&header/beamer` is important.
It loads the precompiled header and has to be in the first line of the document.
If it is not in the first line or even have a space in fron, *main.tex* will not compile.

Last, run `pdflatex main.tex` and have a look in the created file.